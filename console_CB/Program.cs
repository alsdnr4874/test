﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;

namespace console_CB
{
    class Board_control
    {
        SerialPort port;
        /* arg0 : 0 = PWR, 1 = LED PWM */
        int arg0;

        /* arg1 if arg0 = PWR then  0: PC1, 1: PC2, 2: PC3, 3: LED */
        /* arg1 if arg0 = LED then  0: LED 1, 1: LED 2, 2: LED 3, 3: LED  4*/
        int arg1;

        /* arg2 if arg0 = PWR then 0: PWR off, 1: PWR on */
        /* arg2 if arg0 = LED then 0 ~ 10 brughtness */
        int arg2;

        Board_control()
        {
            port = new SerialPort();
            arg0 = -1;
        }


        void set_arg0(string name)
        {
            if (name.Equals("PWR"))
            {
                arg0 = 0;
            }
            else if (name.Equals("LED"))
            {
                arg0 = 1;
            }
            else
            {
                System.Console.Write("PWR of LED only\n");
                Environment.Exit(0);
            }
        }

        void set_arg1_2(string name1, string name2)
        {
            bool flag_1 = false;
            bool flag_2 = false;

            arg1 = Int32.Parse(name1);
            if (arg1<0 || arg1>=4)
            {
                flag_1 = true;
            }
            arg2 = Int32.Parse(name2);
            if (arg0 == 0)
            {
                if(arg2 != 0 && arg2 != 1)
                {
                    flag_2 = true;
                }
            }
            else if(arg0 == 1)
            {
                if(arg2 < 0 || arg2 >10)
                {
                    flag_2 = true;
                }
            }

            if(flag_1 == true || flag_2 ==true)
            {
                System.Console.Write("Value error\n");
                Environment.Exit(0);
            }
        }

        void Port_Open(string portname)
        {
            port.PortName = portname;
            port.BaudRate = 115200;
            port.DataBits = 8;
            port.StopBits = StopBits.One;
            port.Handshake = Handshake.None;
            port.Parity = Parity.None;

            port.Open();
        }

        void Port_Close()
        {
            if (port.IsOpen)
            {
                port.Close();
            }
        }

        void write_serial(byte[] data, int len)
        {
            if (port.IsOpen)
            {
                port.Write(data, 0, len);
            }
        }

        void send_data()
        {
            byte[] buff = new byte[16];
            ushort crc = 0;

            buff[0] = 0x02;
            buff[5] = (byte)(crc & 0xff);
            buff[6] = (byte)(crc >> 8);
            buff[7] = 0x03;

            if(arg0 == 0)
            {
                buff[1] = (byte)'G';
                buff[2] = 6;
                buff[3] = (byte)arg2;
                buff[4] = (byte)arg1;
            }
            else if(arg0 == 1)
            {
                buff[1] = (byte)'L';
                buff[2] = 7;
                buff[3] = 1;
                buff[4] = (byte)arg2;
                buff[5] = (byte)arg1;
                buff[6] = (byte)(crc & 0xff);
                buff[7] = (byte)(crc >> 8);
                buff[8] = 0x03;
            }
            write_serial(buff, 9);

        }

        void wait_response()
        {
            byte[] ch = new byte[1];

            while (true)
            {
                port.Read(ch, 0, 1);

                System.Console.Write(System.Text.Encoding.ASCII.GetString(ch));

                if (ch[0] == '\n')
                    break;
                
            }
            
        }

        static void usages()
        {
            System.Console.Write("use 3 keyword\n");
            Environment.Exit(0);
        }


        static void Main(string[] args)
        {
            if (args.Length < 3)
            {
                usages();
            }
            if (args.Length >= 4)
            {
                usages();
            }
            

            Board_control bd = new Board_control();

            bd.set_arg0(args[0]);
            bd.set_arg1_2(args[1], args[2]);
            
            bd.Port_Open("COM6");

            bd.send_data();

            bd.wait_response();

            bd.Port_Close();


        }
    }
}

